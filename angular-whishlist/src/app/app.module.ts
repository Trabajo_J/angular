import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PreparacionesComponent } from './preparaciones/preparaciones.component';
import { ListaPreparacionesComponent } from './lista-preparaciones/lista-preparaciones.component';

@NgModule({
  declarations: [
    AppComponent,
    PreparacionesComponent,
    ListaPreparacionesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
