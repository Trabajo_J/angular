import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { comidas } from './../models/comidas.modal';

@Component({
  selector: 'app-preparaciones',
  templateUrl: './preparaciones.component.html',
  styleUrls: ['./preparaciones.component.css']
})
export class PreparacionesComponent implements OnInit {
  @Input() preparacion: comidas;
  @HostBinding('attr.class') cssClass = 'col';


  constructor() {}

  ngOnInit(): void {
  }

}
