import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparacionesComponent } from './preparaciones.component';

describe('PreparacionesComponent', () => {
  let component: PreparacionesComponent;
  let fixture: ComponentFixture<PreparacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
