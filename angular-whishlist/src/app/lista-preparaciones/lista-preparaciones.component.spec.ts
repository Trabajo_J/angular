import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPreparacionesComponent } from './lista-preparaciones.component';

describe('ListaPreparacionesComponent', () => {
  let component: ListaPreparacionesComponent;
  let fixture: ComponentFixture<ListaPreparacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPreparacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPreparacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
