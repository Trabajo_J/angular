import { Component, OnInit } from '@angular/core';
import { comidas } from './../models/comidas.modal';

@Component({
  selector: 'app-lista-preparaciones',
  templateUrl: './lista-preparaciones.component.html',
  styleUrls: ['./lista-preparaciones.component.css']
})
export class ListaPreparacionesComponent implements OnInit {
  preparaciones: comidas[];
  constructor() { 
  this.preparaciones = [];
  }

  ngOnInit(): {
  }

  guardar(nombre:string, url:string):boolean {
  this.preparaciones.push(new comidas (nombre,url));
  console.log(this.preparaciones);
  return false;
  }

}
